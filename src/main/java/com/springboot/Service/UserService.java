package com.springboot.Service;

import com.springboot.Dao.ConfirmationTokenDao;
import com.springboot.Dao.UserDao;
import com.springboot.Entity.User;
import com.springboot.search.UserSearchData;
import com.springboot.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class UserService {

    private final UserDao userDao;

    private final ConfirmationTokenDao confirmationTokenDao;

    private final EntityManager em;

    @Autowired
    public UserService(UserDao userDao, ConfirmationTokenDao confirmationTokenDao, EntityManager em) {
        this.userDao = userDao;
        this.confirmationTokenDao = confirmationTokenDao;
        this.em = em;
    }

    public void saveUser(User user){
        userDao.save(user);
    }

    public User findByEmail(String email){
        return userDao.findAllByEmail(email);
    }

    public List<User> findAll(){
        return userDao.findUsers();
    }

    public void deleteById(Integer id){
        confirmationTokenDao.deleteTokenByUserId(id);
        userDao.deleteById(id);
    }

    public Optional<User> findById(Integer id){
        return userDao.findById(id);
    }

    public List<User> findUsersByName(String name) {
        return em.createQuery("select u from User u where lower(u.name) like lower(:name)", User.class)
                .setParameter("name", "%"  + name + "%")
                .getResultList();
    }

    public Iterable<User> findAllByName(String name){
        return userDao.findAllByName(name);
    }

    public Iterable<User> findAllByNameAndSurname(String name, String surname){
        return userDao.findAllByNameAndSurname(name,surname);
    }

    public Iterable<User> findAllBySurname(String surname){
        return userDao.findAllBySurname(surname);
    }

    public List<User> findUsers(UserSearchData userSearchData) {
        Map<String, Object> params = new HashMap<>();
        String filter = getFilter(userSearchData, params);

        Query query = em.createQuery("select u from User u " + filter + " order by u.id desc");

        params.keySet().forEach(s -> query.setParameter(s, params.get(s)));


        return query.getResultList();
    }

    private String getFilter(UserSearchData userSearchData, Map<String, Object> params) {
        String filter = " where 1=1 ";

        if (!Utils.isBlank(userSearchData.getFirstName())) {
            filter += " and lower(u.name) like lower(:firstName) ";
            params.put("firstName", "%" + userSearchData.getFirstName() + "%");
        }

        if (!Utils.isBlank(userSearchData.getLastName())) {
            filter += " and lower(u.surname) like lower(:lastName) ";
            params.put("lastName", "%" + userSearchData.getLastName() + "%");
        }

        if (!Utils.isBlank(userSearchData.getEmail())) {
            filter += " and lower(u.email) like lower(:email) ";
            params.put("email", "%" + userSearchData.getEmail() + "%");
        }

        if (!Utils.isBlank(userSearchData.getGender())) {
            filter += " and lower(u.gender) like lower(:gender) ";
            params.put("gender", "%" + userSearchData.getGender() + "%");
        }

        if (userSearchData.getAge() != null) {
            filter += " and u.age = :age ";
            params.put("age", userSearchData.getAge());
        }
        return filter;
    }

}
