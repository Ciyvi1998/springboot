package com.springboot.Service;

import com.springboot.Dao.RoleDao;
import com.springboot.Entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoleService {

    private final RoleDao roleDao;

    @Autowired
    public RoleService(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    public Optional<Role> findById(Integer id){
        return roleDao.findById(id);
    }
    public List<Role> findAll(){
        return roleDao.findAll();
    }
}
