package com.springboot.Service;

import com.springboot.Dao.UserDao;
import com.springboot.Entity.MyUserDetails;
import com.springboot.Entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Qualifier("myUserDetailsService")
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
         User user = userDao.findAllByEmail(email);

         if(user == null){
             throw new UsernameNotFoundException("მომხმარებელი არ მოიძებნა");
         }

         return new MyUserDetails(user);
    }
}
