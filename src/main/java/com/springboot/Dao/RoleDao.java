package com.springboot.Dao;

import com.springboot.Entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleDao extends JpaRepository<Role, Integer> {
    @Override
    Optional<Role> findById(Integer integer);

    @Override
    List<Role> findAll();
}
