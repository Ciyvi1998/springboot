package com.springboot.Dao;

import com.springboot.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserDao extends CrudRepository<User, Integer> {

    @Query("SELECT u FROM User u where u.email = :email")
    User findAllByEmail(@Param("email") String email);

    @Override
    Optional<User> findById(Integer integer);

    @Query("SELECT u FROM User u where u.id = :id")
    User findUserById(Integer id);

   // @Query("SELECT u FROM User u where u.name = :name")
    Iterable<User> findAllByName(@Param("name") String name);

    Iterable<User> findAllByNameAndSurname(@Param("name") String name,
                                           @Param("surname") String surname);

    Iterable<User> findAllBySurname(@Param("surname") String surname);

    @Query("SELECT u from User u")
    List<User> findUsers();



    @Transactional
    @Modifying
    @Query("delete from User u where u.id = :id")
    void deleteUserById(@Param("id") Integer id);
}
