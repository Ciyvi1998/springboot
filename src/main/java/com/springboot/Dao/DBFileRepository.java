package com.springboot.Dao;

import com.springboot.Entity.DBFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DBFileRepository extends JpaRepository<DBFile, Integer> {
    @Override
    List<DBFile> findAll();

    @Override
    Optional<DBFile> findById(Integer integer);
}