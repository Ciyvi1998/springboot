package com.springboot.Controller;

import com.springboot.Entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WebController {

    @RequestMapping(value = "/")
    public static String index(){
        return "index";
    }

    @RequestMapping(value = "/home")
    public static String home(){
        return "home";
    }

    @RequestMapping(value = "registration/user")
    public static String registerUser(@ModelAttribute("user") User user,
                                      Model model){
        model.addAttribute("user", user);
        return "registerUser";
    }

    @RequestMapping(value = "/403")
    public static String accessDenied(){
        return "403";
    }

    @RequestMapping(value = "/login")
    public static String loginForm(){
        return "login";
    }

    @RequestMapping(value = "/example")
    public static String example(){
        return "example";
    }

    @RequestMapping(value = "/completed")
    public static String completed(){
        return "completed";
    }

    @RequestMapping(value = "/verified")
    public static String  verified(){
        return "verified";
    }

    @RequestMapping(value = "/error/verified")
    public static String  errorVerified(){
        return "errorVerify";
    }

    @RequestMapping(value = "/user/display")
    public static String  displayAllUsers(){
        return "users";
    }

    @RequestMapping(value = "/user/displayByName")
    public static String findAllByName(){
        return "users";
    }

    @RequestMapping(value = "/forgot-password")
    public static String  forgotPassword(){
        return "forgotPassword";
    }

    @RequestMapping(value = "/reset-password")
    public static String  resetPassword(){
        return "resetPassword";
    }

    @RequestMapping(value = "/upload")
    public static String  upload(){
        return "upload";
    }

    @RequestMapping(value = "/upload-status")
    public static String  uploadStatus(){
        return "uploadStatus";
    }

    @RequestMapping(value = "/uploadFiles")
    public static String  uploadFiles(){
        return "uploadFiles";
    }

    @RequestMapping(value = "/displayFiles")
    public static String  displayPhotos(){
        return "displayPhotos";
    }

    @RequestMapping(value = "/test")
    public static String  test(){
        return "test";
    }

//    @RequestMapping(value = "/editUser")
//    public static String  editUser(){
//        return "editUser";
//    }

}
