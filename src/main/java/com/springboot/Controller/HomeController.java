package com.springboot.Controller;

import com.springboot.Entity.Role;
import com.springboot.Entity.User;
import com.springboot.Service.RoleService;
import com.springboot.Service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/service")
public class HomeController {

    private final UserService userService;

    private final RoleService roleService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final Logger log = Logger.getLogger(HomeController.class);

    @Autowired
    public HomeController(UserService userService, RoleService roleService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.roleService = roleService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @RequestMapping(value = "/getUserById/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getUserById(@PathVariable Integer id){

        try {
            User user = userService.findById(id).get();

            log.info("User with email: " + user.getEmail() + " found");
            return ResponseEntity.ok().body(user);
        } catch (Exception e) {
            log.error("User with id: " + id + " didn't find");
            e.printStackTrace();
            return new ResponseEntity("User didn't find", HttpStatus.NOT_FOUND);
        }

    }

    @RequestMapping(value = "/addNewUser", method = RequestMethod.POST)
    public ResponseEntity<?> addNewUser(@RequestBody User user){

        try {
            List<Role> roles = new ArrayList<>();

            roles.add(roleService.findById(2).get());

            user.setRoles(roles);

            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

            userService.saveUser(user);


            log.info("User with email: " + user.getEmail() + " added");
            return new ResponseEntity("User successfully added", HttpStatus.OK);
        } catch (Exception e) {
            log.error("User didn't add");
            e.printStackTrace();
            return new ResponseEntity("User didn't add", HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/getUserByName/{name}", method = RequestMethod.GET)
    public ResponseEntity<?> getUserByName(@PathVariable String name){

        try {
           List<User> users = userService.findUsersByName(name);


            log.info("User with Name: " + name + " found");
            return ResponseEntity.ok().body(users);
        } catch (Exception e) {
            log.error("User didn't find");
            e.printStackTrace();
            return new ResponseEntity("User didn't find", HttpStatus.BAD_REQUEST);
        }

    }


}
