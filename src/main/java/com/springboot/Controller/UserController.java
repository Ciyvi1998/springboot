package com.springboot.Controller;

import com.springboot.Dao.ConfirmationTokenDao;
import com.springboot.Entity.ConfirmationToken;
import com.springboot.Entity.Role;
import com.springboot.Entity.User;
import com.springboot.Service.EmailSenderService;
import com.springboot.Service.RoleService;
import com.springboot.Service.UserService;
import com.springboot.search.UserSearchData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final RoleService roleService;

    private final ConfirmationTokenDao confirmationTokenDao;

    private final EmailSenderService emailSenderService;

    private UserSearchData userSearchData = new UserSearchData();

    private final Logger log = Logger.getLogger(UserController.class);

    @Autowired
    public UserController(UserService userService, BCryptPasswordEncoder bCryptPasswordEncoder, RoleService roleService, ConfirmationTokenDao confirmationTokenDao, EmailSenderService emailSenderService) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.roleService = roleService;
        this.confirmationTokenDao = confirmationTokenDao;
        this.emailSenderService = emailSenderService;
    }

    @RequestMapping(value = "/display", method = RequestMethod.GET)
    public String findAllByName(Model model) {

        model.addAttribute("user", userService.findAll());
        model.addAttribute("searchData", userSearchData);

        System.out.println("Eka's code");
        System.out.println("Eka's_branch");
        return "users";
    }

    @RequestMapping(value = "/findUsers", method = RequestMethod.GET)
    public String findUsers(@ModelAttribute("searchData") UserSearchData userSearchData, Model model) {

        try {
            List<User> users = userService.findUsers(userSearchData);

            model.addAttribute("user", users);


        } catch (Exception e) {
            log.error("Error while searching Users");
            e.printStackTrace();
        }

        return "users";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteById(@RequestParam(value = "id", required = false) Integer id) {

        User user = userService.findById(id).get();

        // user.getRoles().remove(user);
        user.setRoles(null);

        userService.saveUser(user);

        userService.deleteById(id);
        return "redirect:/user/display";
    }

    // get user to edit
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editUser(@RequestParam(value = "id") Integer id, Model model) {
        User userUpdate = userService.findById(id).get();
        model.addAttribute("editUser", userUpdate);

        return "editUser";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String updateUser(@RequestParam(value = "id") Integer id, @ModelAttribute("editUser") User user) {
        User existingUser = userService.findById(id).get();
        user.setRoles(existingUser.getRoles());
        user.setEnable(existingUser.isEnable());
        user.setPassword(existingUser.getPassword());
        user.setGender(existingUser.getGender());

        userService.saveUser(user);

        return "redirect:/user/display";
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveUser(User user,
                           Model model) {

        User existingUser = userService.findByEmail(user.getEmail());
        if (existingUser != null) {
            model.addAttribute("email", "'" + user.getEmail() + "'");
            model.addAttribute("message", ": აღნიშნული ელექტრონული ფოსტა უკვე რეგისტრირებულია");
            //modelAndView.setViewName("error");

            return "errorVerify";
        } else {
            List<Role> roles = new ArrayList<>();

            roles.add(roleService.findById(2).get());

            user.setRoles(roles);

            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

            userService.saveUser(user);

            ConfirmationToken confirmationToken = new ConfirmationToken(user);

            confirmationTokenDao.save(confirmationToken);

            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(user.getEmail());
            mailMessage.setSubject("რეგისტრაცია");
            mailMessage.setFrom("kakauridze.nika@gtu.ge");
            mailMessage.setText("ანგარიშის გასააქტიურებლად გადადით ბმულზე : "
                    + "http://localhost:8080/user/confirm-account?token=" + confirmationToken.getConfirmationToken());

            emailSenderService.sendEmail(mailMessage);

            model.addAttribute("email", user.getEmail());

            // modelAndView.setViewName("successfulRegisteration");


        }

        return "completed";
    }

    @RequestMapping(value = "/confirm-account", method = {RequestMethod.GET, RequestMethod.POST})
    public String confirmUserAccount(Model model, @RequestParam("token") String confirmationToken) {
        ConfirmationToken token = confirmationTokenDao.findByConfirmationToken(confirmationToken);

        if (token != null) {
            User user = userService.findByEmail(token.getUser().getEmail());
            user.setEnable(true);
            userService.saveUser(user);
            // modelAndView.setViewName("accountVerified");
        } else {
            model.addAttribute("message", "The link is invalid or broken!");
            //modelAndView.setViewName("error");


        }

        return "redirect:/verified";
    }

    //მომხმარებლის მიერ მითითებული მეილის მოძებნა, არსებობის შემთხვევაში მელიზე აღსადგენი ლინკის გაგზავნა.
    @RequestMapping(value = "/forgot-password", method = RequestMethod.POST)
    public String forgotPassword(User user) {
        User userExist = userService.findByEmail(user.getEmail());

        if (userExist != null) {
            ConfirmationToken confirmationToken = new ConfirmationToken(userExist);

            confirmationTokenDao.save(confirmationToken);

            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(userExist.getEmail());
            mailMessage.setSubject("აღადგინე პაროლი");
            mailMessage.setFrom("kakauridze.nika@gtu.ge");
            mailMessage.setText("პაროლის აღსადგენად გადადი მოცემულ ბმულზე: " +
                    "http://localhost:8080/user/confirm-reset?token=" + confirmationToken.getConfirmationToken());

            emailSenderService.sendEmail(mailMessage);

            return "redirect:/";

        }
        return "redirect:/";
    }

    //ჩვენი გაგზავნილი token ცვლადის შემოწმება, და დაშვება პაროლის აღდგენაზე.
    @RequestMapping(value = "/confirm-reset", method = {RequestMethod.GET, RequestMethod.POST})
    public String confirmResetToken(@RequestParam("token") String confirmToken) {

        ConfirmationToken token = confirmationTokenDao.findByConfirmationToken(confirmToken);

        if (token != null) {
            User user = userService.findByEmail(token.getUser().getEmail());
            user.setEnable(true);
            userService.saveUser(user);
            return "resetPassword";

        }
        return "redirect:/verified";
    }

    //პაროლის შეცვლა.
    @RequestMapping(value = "/reset-password", method = RequestMethod.POST)
    public String resetPassword(User user) {
        if (user.getEmail() != null) {
            User tokenUser = userService.findByEmail(user.getEmail());
            tokenUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            userService.saveUser(tokenUser);
            return "redirect:/login";
        }
        return null;
    }

    public UserSearchData getUserSearchData() {
        return userSearchData;
    }

    public void setUserSearchData(UserSearchData userSearchData) {
        this.userSearchData = userSearchData;
    }
}
