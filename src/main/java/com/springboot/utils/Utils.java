package com.springboot.utils;

public class Utils {

    public static boolean isBlank(String... strings) {
        if (strings == null) {
            return true;
        }
        for (String string : strings) {
            if (string == null || string.trim().isEmpty()) {
                return true;
            }
        }
        return false;
    }
}
